//
//  MPoem+CoreDataProperties.h
//  RSPautinka
//
//  Created by Сергей on 25.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MPoem.h"

NS_ASSUME_NONNULL_BEGIN

@interface MPoem (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *mobIsDefault;
@property (nullable, nonatomic, retain) NSString *mobPoemContent;
@property (nullable, nonatomic, retain) NSString *mobPoemName;
@property (nullable, nonatomic, retain) id mobImage;
@property (nullable, nonatomic, retain) MAuthor *rlsAuthor;

@end

NS_ASSUME_NONNULL_END
