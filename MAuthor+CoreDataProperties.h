//
//  MAuthor+CoreDataProperties.h
//  RSPautinka
//
//  Created by Сергей on 20.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MAuthor.h"

NS_ASSUME_NONNULL_BEGIN

@interface MAuthor (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *mobAuthorName;
@property (nullable, nonatomic, retain) id mobImage;
@property (nullable, nonatomic, retain) NSSet<MPoem *> *rlsPoem;

@end

@interface MAuthor (CoreDataGeneratedAccessors)

- (void)addRlsPoemObject:(MPoem *)value;
- (void)removeRlsPoemObject:(MPoem *)value;
- (void)addRlsPoem:(NSSet<MPoem *> *)values;
- (void)removeRlsPoem:(NSSet<MPoem *> *)values;

@end

NS_ASSUME_NONNULL_END
