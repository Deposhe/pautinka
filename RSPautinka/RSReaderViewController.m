//
//  RSReaderViewController.m
//  RSPautinka
//
//  Created by Сергей on 19.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSReaderViewController.h"
#import "RSSettingsViewController.h"
#import "RSDataManager.h"
#import "MSettings.h"
#import "ViewController.h"
#import "EditViewController.h"


typedef enum {
    DifficultyLevelFirst,
    DifficultyLevelSecond,
    DifficultyLevelThird,
    DifficultyLevelFourth
            } DifficultyLevel;


NSString * fonSize = @"fontSize";
NSString * fontFamily = @"fontFamily";


@interface RSReaderViewController ()

@property (nonatomic, weak) NSString * chosenContentName;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UILabel *authorTextLabel;
@property (nonatomic, weak) IBOutlet UILabel *poemNameTextLabel;
@property (nonatomic, strong) NSString * basicPoem;
@property (nonatomic, weak) IBOutlet UIImageView *PoemImageVIew;
@property (nonatomic) CGFloat fontFamilySize;
@property (nonatomic, weak) NSString * fontFamilyStyle;

@property (nonatomic, weak) UIImageView * backgroundImageView;

@end

@implementation RSReaderViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect backFrame = self.view.frame;
    self.backgroundImageView = [RSDataManager setImageViewBackgroundWithAlpha:0.2];
    self.backgroundImageView.frame = backFrame;
    [self.view addSubview:self.backgroundImageView];
    
    [self addButtonsOnView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    NSDictionary * settings = [RSDataManager getTextSettings];
    self.fontFamilySize = [[settings valueForKey:fonSize] floatValue];
    self.fontFamilyStyle = [settings valueForKey:fontFamily];
    
    [self.authorTextLabel setFont:[UIFont fontWithName:@"Superclarendon-Regular" size:20.0f]];
    [self addData];
    [self.navigationController.navigationBar setHidden:NO];
    [self.textView setBounces:YES];
    
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Назад"
                                    style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    self.navigationItem.backBarButtonItem = backButton;
    
    
    CGSize itemSize = self.PoemImageVIew.bounds.size;
    UIGraphicsBeginImageContextWithOptions(itemSize, NO, UIScreen.mainScreen.scale);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [self.PoemImageVIew.image drawInRect:imageRect];
    [self.PoemImageVIew.layer setMasksToBounds:YES];
    [self.PoemImageVIew.layer setCornerRadius:20.0f];
    [self.PoemImageVIew.layer setBorderColor:[UIColor redColor].CGColor];
    self.PoemImageVIew.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark data actions

- (void) addData
{
    MPoem * poem =[RSDataManager getChosenPoem];
    MAuthor * author = poem.rlsAuthor;
    
    [self.authorTextLabel setText:author.mobAuthorName];
    [self setBasicPoem:poem.mobPoemContent];
    [self.textView setText:poem.mobPoemContent];
    [self.PoemImageVIew setImage:[UIImage imageWithData:poem.mobImage]];
    [self.poemNameTextLabel setText:poem.mobPoemName];
    
    [self.PoemImageVIew setImage:[UIImage imageWithData:poem.mobImage]];
    [self.textView setFont:[UIFont fontWithName:self.fontFamilyStyle size:self.fontFamilySize]];
}



#pragma mark Text mutations

- (void) separateTextWithDifficulty: (DifficultyLevel) level
{
    CGFloat hideWordsIndex;
    switch (level)
    {
        case DifficultyLevelFirst:
            hideWordsIndex = 0.3;
            break;
            
        case DifficultyLevelSecond:
            hideWordsIndex = 0.5;
            break;
            
        case DifficultyLevelThird:
            hideWordsIndex = 0.6;
            break;
            
        case DifficultyLevelFourth:
            hideWordsIndex = 0.9;
            break;
            
        default:
            break;
    }
    
    NSArray * arrayOfStrings = [self.basicPoem componentsSeparatedByString:@"\n"];
    NSMutableArray * MutableArray = [NSMutableArray array];
    for (NSString * stringOfPoem in arrayOfStrings)
    {
        if (![stringOfPoem  isEqual: @""]) {
            NSArray * arrayFromString = [stringOfPoem componentsSeparatedByString:@" "];
            NSMutableArray * mutArrayFromString = [arrayFromString mutableCopy];
            NSArray * numbersArray = [self getRandomNonRepeatableNumbers: (int)(mutArrayFromString.count * hideWordsIndex) fromArray:mutArrayFromString];
            
            for (NSNumber * number in numbersArray) {
                [mutArrayFromString replaceObjectAtIndex: [number intValue] withObject:@" ... "];
            }
            
            [MutableArray addObjectsFromArray:mutArrayFromString];
            
        }
        [MutableArray addObject:@"\n"];
    }
    
    
    [self animatePoemTextReplacing];
    
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.textView.text = [MutableArray componentsJoinedByString:@" "];
    });
}

- (NSArray *) getRandomNonRepeatableNumbers: (NSInteger) numbers fromArray: (NSArray *) array
{
    NSMutableArray *arrayWithNumbers = [NSMutableArray array];
    
    for (int i=0; i < numbers; i++)
    {
        NSNumber * number;
        BOOL isCorrectNumber = NO;
        
        while (!isCorrectNumber)
        {
            number = [NSNumber numberWithUnsignedInt:arc4random() % (array.count)];
            
            isCorrectNumber = YES;
            
            NSUInteger count = [arrayWithNumbers count];
            for (NSUInteger i = 0; i<count; i++)
            {
                if ([number intValue] == [[arrayWithNumbers objectAtIndex: i] intValue])
                {
                    isCorrectNumber = NO;
                }
            }
        }
        [arrayWithNumbers addObject:number];
    }
   
    return arrayWithNumbers.copy;
}



#pragma mark Animation

- (void) animatePoemTextReplacing
{
    CGFloat duration = 0.5f;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [weakSelf.textView setAlpha:0.0f];
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:duration
                                               delay:0
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              [weakSelf.textView setAlpha:1.0f];
                                          }
                                          completion:^(BOOL finished) {
                                          }];

                     }];
}

- (void) animateButton: (UIButton * ) button {
    CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI * 0.15);
    CGAffineTransform transform2 = CGAffineTransformMakeRotation(M_PI * 2.0);
    
    CGFloat duration = 0.10f;
    
    [UIView animateWithDuration:duration
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [button.layer setTransform:CATransform3DMakeAffineTransform(transform)];
                     }
                     completion:^(BOOL finished) {
                     }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:duration
                              delay:0.0
                            options:UIViewAnimationOptionCurveLinear 
                         animations:^{
                             [button.layer setTransform:CATransform3DMakeAffineTransform(transform2)];
                         }
                         completion:^(BOOL finished) {
                         }];
    });
}



#pragma mark Buttons

- (IBAction)didPressResetButton:(id)sender {
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.textView.text = self.basicPoem;
    });
    [self animatePoemTextReplacing];
}

- (IBAction)didPressFirstLevelButton:(UIButton *) sender{
    [self separateTextWithDifficulty:DifficultyLevelFirst];
    [self animateButton:sender];
}

- (IBAction)didPressSecondLevelButton:(UIButton *) sender{
    [self separateTextWithDifficulty:DifficultyLevelSecond];
    [self animateButton:sender];
}

- (IBAction)didPressThirdLevelButton:(UIButton *) sender{
    [self separateTextWithDifficulty:DifficultyLevelThird];
    [self animateButton:sender];
}

- (IBAction)didPressFourthLevelButton:(UIButton *) sender{
    [self separateTextWithDifficulty:DifficultyLevelFourth];
    [self animateButton:sender];
}

- (IBAction) didPressEditButton:(id)sender{
    
    EditViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([EditViewController class])];
    
    controller.isEditingExisetPoem = YES;
    controller.chosenPoem = [RSDataManager getChosenPoem];
    
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) addButtonsOnView
{
    UIBarButtonItem * editButton = [[UIBarButtonItem alloc] initWithTitle:@"Изменить" style:UIBarButtonItemStylePlain target:self action:@selector(didPressEditButton:)];
    self.navigationItem.rightBarButtonItem = editButton;
    
    CGRect resetButtonFrame = CGRectMake(10, CGRectGetMaxY(self.view.frame) - 80, 70, 70);
    CGRect firstLevelButtonFrame = CGRectMake(CGRectGetMaxX(resetButtonFrame) + 10, CGRectGetMaxY(self.view.frame) - 70, 60, 60);
    CGRect secondLevelButtonFrame = CGRectMake(CGRectGetMaxX(firstLevelButtonFrame) + 10, CGRectGetMaxY(self.view.frame) - 70, 60, 60);
    CGRect thirdLevelButtonFrame = CGRectMake(CGRectGetMaxX(secondLevelButtonFrame) + 10, CGRectGetMaxY(self.view.frame) - 70, 60, 60);
    CGRect fourthLevelButtonFrame = CGRectMake(CGRectGetMaxX(thirdLevelButtonFrame) + 10, CGRectGetMaxY(self.view.frame) - 70, 60, 60);
    
    UIButton * resetButton = [[UIButton alloc] initWithFrame:resetButtonFrame];
    UIButton * firstLevelButton = [[UIButton alloc] initWithFrame:firstLevelButtonFrame];
    UIButton * secondLevelButton = [[UIButton alloc] initWithFrame:secondLevelButtonFrame];
    UIButton * thirdLevelButton = [[UIButton alloc] initWithFrame:thirdLevelButtonFrame];
    UIButton * fourthLevelButton = [[UIButton alloc] initWithFrame:fourthLevelButtonFrame];
    
    [resetButton setImage:[UIImage imageNamed:@"reset.png"] forState:UIControlStateNormal];
    [firstLevelButton setImage:[UIImage imageNamed:@"netLevel1.png"] forState:UIControlStateNormal];
    [secondLevelButton setImage:[UIImage imageNamed:@"netLevel2.png"] forState:UIControlStateNormal];
    [thirdLevelButton setImage:[UIImage imageNamed:@"netLevel3.png"] forState:UIControlStateNormal];
    [fourthLevelButton setImage:[UIImage imageNamed:@"netLevel4.png"] forState:UIControlStateNormal];
    
    [resetButton addTarget:self action:@selector(didPressResetButton:) forControlEvents:UIControlEventTouchUpInside];
    [firstLevelButton addTarget:self action:@selector(didPressFirstLevelButton:) forControlEvents:UIControlEventTouchUpInside];
    [secondLevelButton addTarget:self action:@selector(didPressSecondLevelButton:) forControlEvents:UIControlEventTouchUpInside];
    [thirdLevelButton addTarget:self action:@selector(didPressThirdLevelButton:) forControlEvents:UIControlEventTouchUpInside];
    [fourthLevelButton addTarget:self action:@selector(didPressFourthLevelButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:resetButton];
    [self.view addSubview:firstLevelButton];
    [self.view addSubview:secondLevelButton];
    [self.view addSubview:thirdLevelButton];
    [self.view addSubview:fourthLevelButton];
}

@end
