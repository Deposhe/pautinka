//
//  EditViewController.h
//  RSPautinka
//
//  Created by Сергей on 23.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPoem.h"

@interface EditViewController : UIViewController

@property (nonatomic) BOOL isEditingExisetPoem;
@property (nonatomic, strong) MPoem * chosenPoem;

@end
