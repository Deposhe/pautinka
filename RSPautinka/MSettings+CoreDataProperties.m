//
//  MSettings+CoreDataProperties.m
//  RSPautinka
//
//  Created by Сергей on 05.03.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MSettings+CoreDataProperties.h"

@implementation MSettings (CoreDataProperties)

@dynamic mobFontFamily;
@dynamic mobFontSize;
@dynamic mobSettingsName;
@dynamic mobShowBackground;
@dynamic mobBackgroundImage;

@end
