//
//  RSCollectionViewController.m
//  RSPautinka
//
//  Created by Сергей on 19.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSCollectionViewController.h"
#import "ViewController.h"
#import "MPoem.h"
#import "MAuthor.h"
#import "RSDataManager.h"
#import "RSReaderViewController.h"
#import "EditViewController.h"

@interface RSCollectionViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray * authorsArray;
@property (nonatomic, weak) IBOutlet UITableView *MyTableView;
@property (nonatomic, weak) UISearchBar * searchBar;
@property (nonatomic, strong) NSArray * poemsFilteredArray;
@property (nonatomic) BOOL isFiltered;

@property (nonatomic, weak) UIImageView * backgroundImageView;



@end

@implementation RSCollectionViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    
    
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Назад"
                                    style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    self.navigationItem.backBarButtonItem = backButton;
    [self.navigationItem setTitle:@"Коллекция"];
    
    //---------   back subviews
    CGRect backFrame = self.view.frame;
    self.backgroundImageView = [RSDataManager setImageViewBackgroundWithAlpha:0.2];
    self.backgroundImageView.frame = backFrame;
    [self.view addSubview:self.backgroundImageView];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [self showEmptyCollectionAlert];
    
    [self registerForKeyboardNotification];
    
    [self.navigationController.navigationBar setHidden:NO];
    
    UIBarButtonItem * addCustomPoemBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(didPressAddCustomElementButton:)];
    
    UIBarButtonItem * searchBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(didPressSearchButton:)];
    
    [self setIsFiltered:NO];
    
    self.navigationItem.rightBarButtonItems  = @[addCustomPoemBarButton, searchBarButton];
    
    self.authorsArray = [[RSDataManager getAuthorsFromDataBase] copy];
    [self.MyTableView reloadData];
}



- (void) viewWillDisappear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.searchBar resignFirstResponder];
    [self animateSearchBarDisappeareance];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    MAuthor * author = [self.authorsArray objectAtIndex:section];
    
    if (self.isFiltered == NO)
    {
        return author.rlsPoem.count;
    }
    
    else if (self.isFiltered == YES)
    {
        NSArray * poemTempArray = [self.poemsFilteredArray valueForKeyPath:@"@unionOfObjects.rlsAuthor"];
        
        NSUInteger counter = 0;
        
        if (author != nil)        {
            
            for (MAuthor * tempAuthor in poemTempArray)
            {
                if ([tempAuthor isEqual:author])
                {
                    counter++;
                }
            }
            
            return counter;
        }
    }
    
    return 0;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"Cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    
    MAuthor * author = [self.authorsArray objectAtIndex:indexPath.section];
    NSMutableArray * mutablePoemsArray = [NSMutableArray array];
    
    if (self.isFiltered == NO)
    {
        NSSortDescriptor * sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"mobPoemName" ascending:YES];
        mutablePoemsArray = [[author.rlsPoem.allObjects sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    }
    
    else if (self.isFiltered == YES)
    {
        for (MPoem * tempPoem in self.poemsFilteredArray)
        {
            if ([tempPoem.rlsAuthor isEqual:author])
            {
                [mutablePoemsArray addObject:tempPoem];
            }
        }
    }
    
    MPoem * poem = [mutablePoemsArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = poem.mobPoemName;
    cell.imageView.image = [UIImage imageWithData:poem.mobImage];
    
    CGSize itemSize = CGSizeMake(60, 60);
    UIGraphicsBeginImageContextWithOptions(itemSize, NO, UIScreen.mainScreen.scale);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [cell.imageView.image drawInRect:imageRect];
    [cell.imageView.layer setCornerRadius:8.0f];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return cell;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.authorsArray.count;
}

- (nullable NSString *) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger) section
{
    MAuthor * author = [self.authorsArray objectAtIndex:section];
    return author.mobAuthorName;
}

- (CGFloat) tableView:(UITableView *) tableView heightForRowAtIndexPath:(NSIndexPath *) indexPath
{
    return 75.0f;
}



#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MAuthor * author = [self.authorsArray objectAtIndex:indexPath.section];
    NSMutableArray * poemsMutableArray = [NSMutableArray array];
    
    if (self.isFiltered == NO)
    {
        NSSortDescriptor * sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"mobPoemName" ascending:YES];
        poemsMutableArray = [[author.rlsPoem.allObjects sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    }
    
    else if (self.isFiltered == YES)
    {
        for (MPoem * tempPoem in self.poemsFilteredArray)
        {
            if ([tempPoem.rlsAuthor isEqual:author])
            {
                [poemsMutableArray addObject:tempPoem];
            }
        }
    }
    
    MPoem * poem = [poemsMutableArray objectAtIndex:indexPath.row];
    
    RSDataManager * data = [[RSDataManager alloc] init];
    [data setChosenPoemWithName:poem.mobPoemName];
    
    RSReaderViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RSReaderViewController class])];
    [self.navigationController pushViewController:controller animated:YES];
}


    
#pragma mark - Button Actions

- (IBAction)didPressAddCustomElementButton:(id)sender
{
    EditViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([EditViewController class])];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction) didPressSearchButton:(id) sender
{
    if (self.navigationController.navigationBar.hidden == NO)
    {
        CGRect searchBarFrame = CGRectMake(CGRectGetMaxX(self.view.frame), 20.0f, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
        UISearchBar * searchBar = [[UISearchBar alloc] initWithFrame:searchBarFrame];
        
        [self.view addSubview:searchBar];
        self.searchBar = searchBar;
        self.searchBar.delegate = self;
        [self.searchBar setPlaceholder:@"Поиск по содержимому"];
        
        [self animateSearchBarAppeareance];
        [self.searchBar becomeFirstResponder];
    }
    
}


#pragma mark - UISearchBarDelegate

- (BOOL) searchBarShouldBeginEditing:(UISearchBar *) searchBar
{
    [self.searchBar setShowsCancelButton:YES animated:YES];
    
    // change cancel button title
    for (UIView *view in self.searchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                UIButton *cancelButton = (UIButton*)subview;
                [cancelButton setTitle:@"Отмена" forState:UIControlStateNormal];
                CGRect cancelButtonFrame = CGRectMake(CGRectGetMaxX(self.searchBar.frame) - 72.0, 0.0, 70.0, CGRectGetHeight(self.searchBar.frame));
                [cancelButton setFrame:cancelButtonFrame];
            }
        }
    }
    
    return YES;
}


- (void) searchBar:(UISearchBar *) searchBar textDidChange:(NSString *) searchText{
    
    if ([searchText isEqualToString:@""])
    {
        self.authorsArray = [[RSDataManager getAuthorsFromDataBase] mutableCopy];
        
        if (self.isFiltered == YES)
        {
            [self setIsFiltered:NO];
            [self.MyTableView reloadData];
        }
        return;
    }
    
    [self setIsFiltered:YES];
    
    
    
    self.poemsFilteredArray = [RSDataManager searchPoemText:searchText];
    
    self.authorsArray = [self.poemsFilteredArray valueForKeyPath:@"@distinctUnionOfObjects.rlsAuthor"];
    NSSortDescriptor * sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"mobAuthorName" ascending:YES];
    self.authorsArray = [[self.authorsArray sortedArrayUsingDescriptors:@[sortDescriptor]] copy];
    [self.MyTableView reloadData];
   
}

- (BOOL) searchBarShouldEndEditing:(UISearchBar *) searchBar
{
    [self.searchBar resignFirstResponder];
    return YES;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [self setIsFiltered:NO];
    [self.searchBar resignFirstResponder];
    self.authorsArray = [[RSDataManager getAuthorsFromDataBase] mutableCopy];
    [self.MyTableView reloadData];
    [self animateSearchBarDisappeareance];
    
}



#pragma mark - Animations

- (void) animateSearchBarAppeareance
{
    [UIView  animateWithDuration:0.40f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        CGRect searchBarFrame = CGRectMake(0.0f, 20.0f, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
        [self.searchBar setFrame:searchBarFrame];
        
        CGRect navBarFrame = CGRectMake(-CGRectGetMaxX(self.view.frame), 20.0f, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
        [self.navigationController.navigationBar setFrame:navBarFrame];
        
    } completion: nil];
}

- (void) animateSearchBarDisappeareance
{
    [UIView  animateWithDuration:0.40f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [self.navigationController.navigationBar setHidden:NO];
        
        CGRect searchBarFrame = CGRectMake(CGRectGetMaxX(self.view.frame), 20.0f, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
        [self.searchBar setFrame:searchBarFrame];
        
        CGRect navBarFrame = CGRectMake( 0.0f, 20.0f, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height);
        [self.navigationController.navigationBar setFrame:navBarFrame];
        
    } completion: nil];
    
}



#pragma mark - Keyboard appearance notifications

- (void) registerForKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardWillHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}


- (void) keyBoardWasShown: (NSNotification *) notification
{
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(64.0, 0.0, keyboardSize.height, 0.0);
    self.MyTableView.contentInset = contentInsets;
    self.MyTableView.scrollIndicatorInsets = contentInsets;
}

- (void) keyBoardWillHidden: (NSNotification *) notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.MyTableView.contentInset = UIEdgeInsetsMake(64.0, 0.0, 0.0, 0.0);
    self.MyTableView.scrollIndicatorInsets = contentInsets;
}



#pragma mark - Empty Collection Alert

- (void) showEmptyCollectionAlert
{
    
    if ([[RSDataManager getAuthorsFromDataBase] count] > 0)
    {
        return;
    }
    
    UIAlertController * alertController = [UIAlertController
                                          alertControllerWithTitle:@"Нет доступных колллекций"
                                          message:@"Добавьте стартовую коллекцию или создайте свою"
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction * defaultDataAlertAction = [UIAlertAction actionWithTitle:@"Добавить базовую коллекцию"
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                                           [[RSDataManager alloc] addDefaultDataToDataBase];
                                                                           self.authorsArray = [[RSDataManager getAuthorsFromDataBase] copy];
                                                                           [self.MyTableView reloadData];
                                                                        }];
    
    UIAlertAction * custompoemAlertAction = [UIAlertAction actionWithTitle:@"Создать свою коллекцию"
                                                                         style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                                           [self didPressAddCustomElementButton:nil];
                                                                       }];
    
    UIAlertAction * cancelAlertAction = [UIAlertAction actionWithTitle:@"Отмена"
                                                                        style:UIAlertActionStyleCancel
                                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                                          [self.navigationController popViewControllerAnimated:YES];
                                                                      }];
    
    [alertController addAction:defaultDataAlertAction];
    [alertController addAction:custompoemAlertAction];
    [alertController addAction:cancelAlertAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
