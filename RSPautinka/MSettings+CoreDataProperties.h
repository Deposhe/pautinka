//
//  MSettings+CoreDataProperties.h
//  RSPautinka
//
//  Created by Сергей on 05.03.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MSettings.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSettings (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *mobFontFamily;
@property (nullable, nonatomic, retain) NSString *mobFontSize;
@property (nullable, nonatomic, retain) NSString *mobSettingsName;
@property (nullable, nonatomic, retain) NSNumber *mobShowBackground;
@property (nullable, nonatomic, retain) NSNumber *mobBackgroundImage;

@end

NS_ASSUME_NONNULL_END
