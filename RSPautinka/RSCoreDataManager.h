//
//  RSDataViewController.h
//  RSPautinka
//
//  Created by Сергей on 19.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface RSCoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


+ (RSCoreDataManager *) sharedManager;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end
