//
//  RSSettingsViewController.m
//  RSPautinka
//
//  Created by Сергей on 19.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSSettingsViewController.h"
#import "ViewController.h"
#import "RSSettingsViewController.h"
#import "RSDataManager.h"
#import <UIKit/UIKit.h>


@interface RSSettingsViewController () <UIPickerViewDelegate, UIPickerViewDataSource>


@property (weak, nonatomic) IBOutlet UIPickerView *fontSizePicker;
@property (nonatomic) NSArray * fontSizeArray;
@property (weak, nonatomic) IBOutlet UIPickerView *fontFamilyPicker;
@property (nonatomic) NSArray * fontFamilyArray;

@property (nonatomic, weak) IBOutlet UISwitch * backgroungImageSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl * backgroundSegmentedControl;

@property (nonatomic, strong) NSString * selectedFontSize;
@property (nonatomic, strong) NSString * selectedFontFamily;

@property (nonatomic, weak) UIImageView * backgroundImageView;

@end




@implementation RSSettingsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.fontSizeArray = @[@"12",
                           @"13",
                           @"14",
                           @"15",
                           @"16",
                           @"17",
                           @"18",
                           @"19",
                           @"20",
                           @"21"];
    
    self.fontFamilyArray = @[@"Superclarendon-Regular",
                             @"AppleSDGothicNeo-Medium",
                             @"GeezaPro",
                             @"TimesNewRomanPSMT",
                             @"Verdana",
                             @"ArialHebrew"];
    
    UIBarButtonItem * doneBarButton= [[UIBarButtonItem alloc] initWithTitle:@"Сохранить"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(didPressDoneButton:)];
    
    self.navigationItem.rightBarButtonItem = doneBarButton;
   
    
    [self.backgroundSegmentedControl setTitle:@"Паутина" forSegmentAtIndex:0];
    [self.backgroundSegmentedControl setTitle:@"Доджь" forSegmentAtIndex:1];
    [self.backgroundSegmentedControl setTitle:@"Тетрадь" forSegmentAtIndex:2];
    
    
    CGRect backFrame = self.view.frame;
    self.backgroundImageView = [RSDataManager setImageViewBackgroundWithAlpha:0.2];
    self.backgroundImageView.frame = backFrame;
    [self.view addSubview:self.backgroundImageView];
}

-(void)viewWillAppear:(BOOL)animated
{
   
    
    [self.navigationController.navigationBar setHidden:NO];
    // load settings
    NSDictionary * settingsDictionary = [RSDataManager getTextSettings];
    
    NSUInteger fontSizePosition = [self.fontSizeArray indexOfObject:[settingsDictionary valueForKey:@"fontSize"]];
    NSUInteger fontFamilyPosition = [self.fontFamilyArray indexOfObject:[settingsDictionary valueForKey:@"fontFamily"]];
    
    [self.fontSizePicker selectRow: fontSizePosition inComponent:0 animated:YES];
    [self.fontFamilyPicker selectRow: fontFamilyPosition inComponent:0 animated:YES];
    
    self.selectedFontSize = [self.fontSizeArray objectAtIndex:fontSizePosition];
    self.selectedFontFamily = [self.fontFamilyArray objectAtIndex:fontFamilyPosition];
    
    [self.backgroundSegmentedControl setSelectedSegmentIndex:[[settingsDictionary valueForKey:@"backgroundImage"] integerValue]];
    [self.backgroungImageSwitch setOn: [[settingsDictionary valueForKey:@"showBackgroundImage"] boolValue]];
    
    
    [self.backgroungImageSwitch addTarget:self
                                   action:@selector(didChangeSwitchState:)
                         forControlEvents:UIControlEventValueChanged];
    
    [self chooseSegmentControlStateBySwitch:self.backgroungImageSwitch];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    NSInteger count;
    
    switch (pickerView.tag) {
        case 0:
            count = [self.fontSizeArray  count];
            break;
            
       case 1:
            count = [self.fontFamilyArray  count];
            break;
            
        default:
            break;
    }
    return count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    NSString * pickerRowString;
    switch (thePickerView.tag) {
        case 0:
            pickerRowString = [self.fontSizeArray objectAtIndex:row];
            break;
            
        case 1:
            pickerRowString = [self.fontFamilyArray objectAtIndex:row];
            break;
            
        default:
            break;
    }
    
    return pickerRowString;
}



#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    switch (pickerView.tag) {
        case 0:
            self.selectedFontSize = [self.fontSizeArray objectAtIndex:row];
            break;
            
        case 1:
            self.selectedFontFamily = [self.fontFamilyArray objectAtIndex:row];
            break;
            
        default:
            break;
    }
    
}



#pragma mark - Action Buttons

- (IBAction) didPressDoneButton:(id)sender
{
    RSDataManager * data = [[RSDataManager alloc] init];
    
    [data saveSettingsWithFontFamily:self.selectedFontFamily
                         andFontSize:self.selectedFontSize
                   andShowBackground:[NSNumber numberWithBool: [self.backgroungImageSwitch isOn]]
                  andBackgroundImage:self.backgroundSegmentedControl.selectedSegmentIndex];
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) didPressLoadDefaultCollectionButton:(UIButton *)sender
{
    RSDataManager * data = [[RSDataManager alloc] init];
    [data addDefaultDataToDataBase];
    [sender setHidden:YES];
    
}

- (IBAction) didPressRemoveCollections:(id)sender
{
   UIAlertController * alertController = [UIAlertController
                                           alertControllerWithTitle:@"Удаление коллекции"
                                           message:@"Вы действительно хотите удалить всю коллекцию?"
                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * removeAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Удалить", @"remove action")
                                    style:UIAlertActionStyleDestructive
                                    handler:^(UIAlertAction *action)
                                    {
                                        [RSDataManager removeAllCollections];
                                        [sender setHidden:YES];                                        
                                    }];
    
    UIAlertAction * cancelAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Отмена", @"Cancel action")
                                    style:UIAlertActionStyleCancel
                                    handler:nil];
    
    
    [alertController addAction:removeAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    }



#pragma mark - Switch and SegmentControl relations

- (IBAction) didChangeSwitchState:(id)sender
{
    [self chooseSegmentControlStateBySwitch:self.backgroungImageSwitch];
}

- (void) chooseSegmentControlStateBySwitch: (UISwitch *) switcher
{
    switch ([switcher isOn] ? 1 : 0)
    {
        case 1:
            
            for (NSUInteger i = 0; i < [self.backgroundSegmentedControl numberOfSegments]; i++)
            {
                [self.backgroundSegmentedControl setEnabled:YES forSegmentAtIndex:i];
            }
            
            NSUInteger afterSwitchSecelectedNumber = [[[RSDataManager getTextSettings] valueForKey:@"backgroundImage"] integerValue];
            
            if (afterSwitchSecelectedNumber == UISegmentedControlNoSegment)
            {
                afterSwitchSecelectedNumber = 0;
            }
            
            [self.backgroundSegmentedControl setSelectedSegmentIndex:afterSwitchSecelectedNumber];
            
            break;
            
        case 0:
            for (NSUInteger i = 0; i < [self.backgroundSegmentedControl numberOfSegments]; i++)
            {
                [self.backgroundSegmentedControl setEnabled:NO forSegmentAtIndex:i];
            }
        default:
            break;
    }
}


@end
