//
//  MSettings.h
//  RSPautinka
//
//  Created by Сергей on 05.03.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MSettings : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MSettings+CoreDataProperties.h"
