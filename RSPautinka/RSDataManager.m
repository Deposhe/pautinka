//
//  RSDataManager.m
//  RSPautinka
//
//  Created by Сергей on 20.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "RSDataManager.h"

static NSString * defaultSettingsNameString = @"MainSettings";

@implementation RSDataManager



#pragma mark - Adding Default Data Colection

- (void) addDefaultDataToDataBase
{
     NSString *authorsListPath = [[NSBundle mainBundle] pathForResource:@"defaultsAuthorsLIst" ofType:@"txt"];
     NSString *authorsListContent = [NSString stringWithContentsOfFile:authorsListPath encoding:NSUTF8StringEncoding error:NULL];
     NSArray * authors = [authorsListContent componentsSeparatedByString:@"\n"];
    
    
    for (NSString * defaultAuthorName in authors)
    {
        MAuthor * defAuthor = [self addAuthorWithName:defaultAuthorName];
        
        
        if (defAuthor)
        {
            NSString * poemListPath = [[NSBundle mainBundle] pathForResource:defAuthor.mobAuthorName ofType:@"txt"];
            NSString * poemListContent = [NSString stringWithContentsOfFile:poemListPath encoding:NSUTF8StringEncoding error:NULL];
            NSArray * poemsList = [poemListContent componentsSeparatedByString:@"\n"];
            
            
            for (NSString * defaultPoemName in poemsList)
            {
                NSString * poemContentPath = [[NSBundle mainBundle] pathForResource:defaultPoemName ofType:@"txt"];
                NSString * poemListContent = [NSString stringWithContentsOfFile:poemContentPath encoding:NSUTF8StringEncoding error:NULL];
                UIImage * image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", defaultPoemName]];
                
                MPoem * defaultPoem = [self addPoemWithName:defaultPoemName content:poemListContent andAuthor:defAuthor andImage:image];
                
                if (defaultPoem)
                {
                    [defAuthor  addRlsPoemObject:defaultPoem];
                }
            }
            
        }
    }
    
    [[RSCoreDataManager sharedManager] saveContext];
}



#pragma mark - Authors and poems data actions

- (MAuthor *) addAuthorWithName: (NSString *) authorName{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
   
    //----- request for check existed author
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MAuthor class])];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobAuthorName = %@", authorName];
    NSError * error = nil;
    [request setPredicate:predicate];
    
    MAuthor * author = [[context executeFetchRequest:request error:&error] lastObject];
    
    if (author == nil)
    {
        author = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MAuthor class]) inManagedObjectContext:context];
    }
       
    
    if (author != nil)
    {
        author.mobAuthorName = authorName;    
        
    }
    
    return author;
}

- (MPoem *) addPoemWithName: (NSString *) poemName content: (NSString *) content andAuthor: (MAuthor *) author andImage: (UIImage *) image
{
    NSManagedObjectContext *context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    MPoem * poem = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MPoem class])inManagedObjectContext:context];
    
    if (poem != nil) {
        poem.mobPoemName = poemName;
        poem.mobPoemContent = content;
        poem.rlsAuthor = author;
        poem.mobIsDefault = @0;
        poem.mobImage = UIImageJPEGRepresentation(image, 1.0f);
    }
    
    return poem;
}

+ (MPoem *) getChosenPoem
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MPoem class])];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobIsDefault = %@", @1];
    [fetchRequest setPredicate:predicate];
    
    NSError * error = nil;
    MPoem * poem  = [[context executeFetchRequest:fetchRequest error:&error] lastObject];
    
    return poem;
}

- (void) setChosenPoemWithName: (NSString *) name {
    [self resetDefaultPoem];
    
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MPoem class])];
    
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobPoemName = %@", name];
    [fetchRequest setPredicate:predicate];
    
    NSError * error = nil;
    MPoem * poem  = [[context executeFetchRequest:fetchRequest error:&error] lastObject];
    
    poem.mobIsDefault = @1;
    
    [[RSCoreDataManager sharedManager] saveContext];
}

- (void) resetDefaultPoem {
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MPoem class])];
    
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobIsDefault = %@", @1];
    [fetchRequest setPredicate:predicate];
    
    NSError * error = nil;
    NSArray * tempArray  = [context executeFetchRequest:fetchRequest error:&error];
   
    
    for (MPoem * poem in tempArray)
    {
        poem.mobIsDefault = @0;
    }
   
    [[RSCoreDataManager sharedManager] saveContext];
}

+ (NSArray *) getAuthorsFromDataBase
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MAuthor class])];
    
    NSSortDescriptor * firstSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mobAuthorName" ascending:YES];
    NSArray * sortDescriptorsArray = @[firstSortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptorsArray];
    [fetchRequest setFetchBatchSize:15];
    
    NSError * error = nil;
    NSArray * authorsArray = [context executeFetchRequest:fetchRequest error:&error];
    

    return authorsArray;
}

- (void) saveEditedPoemWithOldPoem: (MPoem *) oldPoem andNewPoem: (MPoem *) newPoem
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MPoem class])];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobPoemName == %@ AND rlsAuthor == %@", oldPoem.mobPoemName, oldPoem.rlsAuthor];
    
    [fetchRequest setPredicate:predicate];
    
    NSError * error = nil;
    
    MPoem * tempPoem = [[context executeFetchRequest:fetchRequest error:&error] lastObject];
    tempPoem = newPoem;
    
    [self removePoem:oldPoem];
    
    [[RSCoreDataManager sharedManager] saveContext];
}

- (void) removePoem: (MPoem *) poem
{
    if (poem == nil)
    {
        NSLog(@"nothing to remove");
        return;
    }
    
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    [context deleteObject:poem];
    
    MAuthor * author = poem.rlsAuthor;
    
    if ([author.rlsPoem.allObjects count] == 1)
    {
        [context deleteObject:author];
    }
    
    [[RSCoreDataManager sharedManager] saveContext];
}

+ (void) removeAllCollections
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MPoem class])];
    NSBatchDeleteRequest * deleteRequest = [[NSBatchDeleteRequest alloc] initWithFetchRequest:fetchRequest];
    NSError * error = nil;
    
    [[[RSCoreDataManager sharedManager] persistentStoreCoordinator] executeRequest:deleteRequest withContext:context error:&error];
    
    NSFetchRequest * secondRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MAuthor class])];
    NSBatchDeleteRequest * secondDeleteRequest = [[NSBatchDeleteRequest alloc] initWithFetchRequest:secondRequest];
    
    
    [[[RSCoreDataManager sharedManager] persistentStoreCoordinator] executeRequest:secondDeleteRequest withContext:context error:&error];
    
    
        
    [[RSCoreDataManager sharedManager] saveContext];
}



#pragma mark - save load settings

+ (void) createDefaultSettings
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MSettings class])];
    
    NSError * error = nil;
    NSArray * tempArray = [context executeFetchRequest:request error:&error];
    
    if (tempArray.count == 0)
    {
        MSettings * settings = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([MSettings class]) inManagedObjectContext:context];
        settings.mobSettingsName = defaultSettingsNameString;
        settings.mobFontFamily = @"Verdana";
        settings.mobFontSize = @"15";
        settings.mobBackgroundImage = @2;
        settings.mobShowBackground = @1;
    }
    else
    {
        return;
    }
        
    [[RSCoreDataManager sharedManager] saveContext];    
}

- (void) saveSettingsWithFontFamily: (NSString *) fontFamily andFontSize: (NSString *) fontSize andShowBackground: (NSNumber *) showBackgroundImage andBackgroundImage: (NSInteger) backgroundImage
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MSettings class])];
    NSError * error = nil;
    
    NSString * defaultSettingsName = defaultSettingsNameString;
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobSettingsName = %@", defaultSettingsName];
    [request setPredicate:predicate];
    
    
    MSettings * mainSettings = [[context executeFetchRequest:request error:&error] lastObject];

    
    if (mainSettings != nil)
    {
        mainSettings.mobFontSize = fontSize;
        mainSettings.mobFontFamily = fontFamily;
        mainSettings.mobShowBackground = showBackgroundImage;
        mainSettings.mobBackgroundImage = [NSNumber numberWithInteger: backgroundImage];
        
    }
    
    [[RSCoreDataManager sharedManager] saveContext];
}

+ (NSDictionary *) getTextSettings
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MSettings class])];
    //--
    NSString * defaultSettingsName = @"MainSettings";
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"mobSettingsName = %@", defaultSettingsName];
    [fetchRequest setPredicate:predicate];
    
    
    NSError * error = nil;
    MSettings * settings  = [[context executeFetchRequest:fetchRequest error:&error] lastObject] ;
    
    NSDictionary * settingsDictionnary = @{@"fontSize"              : settings.mobFontSize,
                                           @"fontFamily"            : settings.mobFontFamily,
                                           @"showBackgroundImage"   : settings.mobShowBackground,
                                           @"backgroundImage"       : settings.mobBackgroundImage};
    return settingsDictionnary;
}



#pragma mark - Background Image Settings

+ (UIImageView *) setImageViewBackgroundWithAlpha: (CGFloat) alpha
{
    NSDictionary * settings = [RSDataManager getTextSettings];
    
    if ([[settings valueForKey:@"showBackgroundImage"] boolValue] == NO )
    {
        return nil;
    }

    UIImageView * imageView = [[UIImageView alloc] init];
    
    UIImage * firstImage = [UIImage imageNamed:@"mainViewImage.jpg"];
    UIImage * secondImage = [UIImage imageNamed:@"rainFont.jpeg"];
    UIImage * thirdImage = [UIImage imageNamed:@"sheetFont.jpeg"];
    NSArray * imageArray = @[firstImage, secondImage, thirdImage];
    
    
    imageView.image = [imageArray objectAtIndex:[[settings valueForKey:@"backgroundImage"] integerValue]];
    [imageView setAlpha:alpha];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    return imageView;
}



#pragma mark - search

+ (NSMutableArray *) searchPoemText:(NSString *)searchText
{
    NSManagedObjectContext * context = [[RSCoreDataManager sharedManager] managedObjectContext];
    
    
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([MPoem class])];
    
    NSPredicate * predicate = [[NSPredicate alloc] init];
    
    predicate = [NSPredicate predicateWithFormat:@"mobPoemContent CONTAINS[cd] %@", searchText];;
    
    NSSortDescriptor * firstDescriptor = [[NSSortDescriptor alloc] initWithKey:@"mobPoemName" ascending:YES];
    NSArray * descriptorsArray = @[firstDescriptor];
    
    [fetchRequest setSortDescriptors:descriptorsArray];
    [fetchRequest setFetchBatchSize:15];
    [fetchRequest setPredicate:predicate];
    
    NSError * error = nil;
    
    NSArray * arrayOfPoems = [context executeFetchRequest:fetchRequest error:&error];
    
    return [arrayOfPoems mutableCopy];
}


@end
