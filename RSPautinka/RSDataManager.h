//
//  RSDataManager.h
//  RSPautinka
//
//  Created by Сергей on 20.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSCoreDataManager.h"
#import "MPoem.h"
#import "MAuthor.h"
#import "MSettings.h"
#import <UIKit/UIKit.h>


@interface RSDataManager : NSObject


- (void) addDefaultDataToDataBase;

- (MAuthor *) addAuthorWithName: (NSString *) authorName;
- (MPoem *) addPoemWithName: (NSString *) poemName content: (NSString *) content andAuthor: (MAuthor *) author andImage: (UIImage *) image;

- (void) setChosenPoemWithName: (NSString *) name;
+ (MPoem *) getChosenPoem;

+ (NSArray *) getAuthorsFromDataBase;

- (void) saveSettingsWithFontFamily: (NSString *) fontFamily andFontSize: (NSString *) fontSize andShowBackground: (NSNumber *) showBackgroundImage andBackgroundImage: (NSInteger) backgroundImage;
+ (NSDictionary *) getTextSettings;
+ (void) createDefaultSettings;

+ (NSMutableArray *) searchPoemText:(NSString *)searchText;

- (void) saveEditedPoemWithOldPoem: (MPoem *) oldPoem andNewPoem: (MPoem *) newPoem;
- (void) removePoem: (MPoem *) poem;
+ (void) removeAllCollections;

+ (UIImageView *) setImageViewBackgroundWithAlpha: (CGFloat) alpha;





@end
