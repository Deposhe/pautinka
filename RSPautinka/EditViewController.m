//
//  EditViewController.m
//  RSPautinka
//
//  Created by Сергей on 23.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "EditViewController.h"
#import "RSDataManager.h"
#import "MPoem.h"
#import "MAuthor.h"
#import "ViewController.h"
#import "RSCollectionViewController.h"

static NSString * placeholderText = @"Добавьте текст.\nСтроки разделяйте абзацами, а слова - пробелами.";

@interface EditViewController () <UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UITextField *authorNameTextField;
@property (nonatomic, weak) IBOutlet UITextField *poemNameTextField;
@property (nonatomic, weak) IBOutlet UITextView *poemContentTextView;
@property (nonatomic, strong) IBOutlet UIImageView *chosenImageView;
@property (nonatomic, strong) UIImage * chosenImage;
@property (nonatomic, strong) UIImagePickerController * pickerController;

@property (nonatomic, weak) UIImageView * backgroundImageView;


@end

@implementation EditViewController



- (void)viewDidLoad
{
    [super viewDidLoad];    
    
    CGRect backFrame = self.view.frame;
    self.backgroundImageView = [RSDataManager setImageViewBackgroundWithAlpha:0.2];
    self.backgroundImageView.frame = backFrame;
    [self.view addSubview:self.backgroundImageView];
    
    [self.poemContentTextView.layer setBorderWidth:3.0f];
    [self.poemContentTextView.layer setCornerRadius:8.0f];
    [self.poemContentTextView.layer setBorderColor:[UIColor colorWithRed:211/255.0 green:211/255.0 blue:211/255.0 alpha:1.0].CGColor];
    
    [self registerForKeyboardNotification];
    
    [self setChosenPoemDataOnView];
        
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    UIBarButtonItem * doneBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить" style:UIBarButtonItemStyleDone target:self action:@selector(didPressDoneBarButton:)];
    UIBarButtonItem * removeBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(didPressRemoveButton:)];
    [removeBarButton setTintColor:[UIColor redColor]];
    [self.navigationItem setRightBarButtonItems:@[doneBarButton, removeBarButton]];
    
    if (self.isEditingExisetPoem == NO)
    {
        [self.poemContentTextView setText:placeholderText];
        [self.poemContentTextView setTextColor:[UIColor lightGrayColor]];
        [removeBarButton setEnabled:NO];

    }    
    
    // graphics
    UIGraphicsBeginImageContextWithOptions(self.chosenImageView.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [[UIBezierPath bezierPathWithRoundedRect:self.chosenImageView.bounds
                                cornerRadius:20.0] addClip];
    
    [self.chosenImage drawInRect:self.chosenImageView.bounds];
    self.chosenImageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void) setChosenPoemDataOnView
{
    
    if (self.isEditingExisetPoem)
    {
        [self.authorNameTextField setText:[self.chosenPoem.rlsAuthor valueForKey:@"mobAuthorName"]];
        [self.poemNameTextField setText:self.chosenPoem.mobPoemName];
        [self.poemContentTextView setText:self.chosenPoem.mobPoemContent];
        [self setChosenImage:[UIImage imageWithData:self.chosenPoem.mobImage]];
        [self.chosenImageView setImage:self.chosenImage];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString: placeholderText]) {
        textView.text = @"";
        textView.textColor = [UIColor lightGrayColor];
    }
    
    [textView becomeFirstResponder];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = placeholderText;
        textView.textColor = [UIColor lightGrayColor];
    }
    
    [textView resignFirstResponder];
}



#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    self.chosenImage = nil;
    self.chosenImage = info[UIImagePickerControllerOriginalImage];
    [self.chosenImageView setImage:self.chosenImage];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.chosenImageView.layer setCornerRadius:8.0f]; // dn work
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark - Actions on buttons

- (IBAction)didPressDoneBarButton:(id)sender
{
    if ([self.authorNameTextField.text isEqualToString:@""] ||
        [self.poemNameTextField.text isEqualToString:@""] ||
        [self.poemContentTextView.text isEqualToString:@""] ||
        [self.poemContentTextView.text isEqualToString:placeholderText])
    {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Все данные должны быть заполнены"
                                                                        message:@"Название, автор, содержание"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
       
        return;
    }
    
    RSDataManager  * data = [[RSDataManager alloc] init];
    
    MAuthor * author = [data addAuthorWithName:self.authorNameTextField.text];
    MPoem * newPoem = [data addPoemWithName:self.poemNameTextField.text content:self.poemContentTextView.text andAuthor:author andImage:self.chosenImage];

    if (self.isEditingExisetPoem == NO)
    {        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (self.isEditingExisetPoem == YES)
    {
        [data saveEditedPoemWithOldPoem:self.chosenPoem andNewPoem:newPoem];
        
        NSArray * navigationControllers = self.navigationController.viewControllers;
        for (NSUInteger i = 0; i < [navigationControllers count]; i++)
        {
            id controller = [navigationControllers objectAtIndex:i];
            
            if ([controller isKindOfClass:[RSCollectionViewController class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    [self.navigationController popToRootViewControllerAnimated:YES]; //to root for uninitialized collectionController
    }
    
    
    
    
}

- (IBAction)didPressSelectImage:(id)sender
{
    self.pickerController = [[UIImagePickerController alloc] init];
    self.pickerController.delegate = self;
    [self.pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self presentViewController:self.pickerController animated:YES completion:nil];
}

- (void) didPressRemoveButton: (id) sender
{
    UIAlertController * alertController = [UIAlertController
                                          alertControllerWithTitle:@"Удаление"
                                                           message:@"Вы действительно хотите удалить элемент из коллекции?"
                                                    preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * removeAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Удалить", @"remove action")
                                        style:UIAlertActionStyleDestructive
                                      handler:^(UIAlertAction *action)
                                    {
                                       [[RSDataManager alloc] removePoem:self.chosenPoem];
                                        
                                        NSArray * navigationControllers = self.navigationController.viewControllers;
                                        for (NSUInteger i = 0; i < [navigationControllers count]; i++)
                                        {
                                            id controller = [navigationControllers objectAtIndex:i];
                                            if ([controller isKindOfClass:[RSCollectionViewController class]])
                                            {
                                                [self.navigationController popToViewController:controller animated:YES];
                                            }
                                        }
                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                        
                                    }];
    
    UIAlertAction * cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Отмена", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:nil];     
    
    [alertController addAction:cancelAction];
    [alertController addAction:removeAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Keyboard appearance notifications

- (void) registerForKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyBoardWillHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}


- (void) keyBoardWasShown: (NSNotification *) notification
{
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    self.poemContentTextView.contentInset = contentInsets;
    self.poemContentTextView.scrollIndicatorInsets = contentInsets;
}

- (void) keyBoardWillHidden: (NSNotification *) notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.poemContentTextView.contentInset = UIEdgeInsetsZero;
    self.poemContentTextView.scrollIndicatorInsets = contentInsets;
}



@end
