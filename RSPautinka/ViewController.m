//
//  ViewController.m
//  RSPautinka
//
//  Created by Сергей on 19.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "ViewController.h"
#import "RSSettingsViewController.h"
#import "RSReaderViewController.h"
#import "RSCollectionViewController.h"
#import "MSettings.h"
#import "RSDataManager.h"




@interface ViewController ()


@property (nonatomic, weak) UIButton * readButton;
@property (nonatomic) BOOL showReadButton;
@property (nonatomic, weak) UIImageView * backgroundImageView;



@end

@implementation ViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [RSDataManager createDefaultSettings];
    [self addMainBackgroundImage];
    [self addButtons];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:YES];
    
    self.showReadButton = [RSDataManager getChosenPoem];
    
    if (self.showReadButton == YES)
    {
        [self.readButton setHidden:NO];
    }
    else
    {
        [self.readButton setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Actions

- (IBAction)didPressReadButton:(id)sender
{
    RSReaderViewController * readController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RSReaderViewController class])];
    [self.navigationController pushViewController:readController animated:YES];
}


- (IBAction)didPressCollectonButton:(id)sender
{
    RSCollectionViewController * collectionController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RSCollectionViewController class])];
    [self.navigationController pushViewController:collectionController animated:YES];
}


- (IBAction)didPressSettingsButton:(id)sender
{
    RSSettingsViewController * settingsController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([RSSettingsViewController class])];
    [self.navigationController pushViewController:settingsController animated:YES];
}



#pragma mark - Adding Interface Elements

- (void) addButtons
{
    CGRect readButtonFrame  = CGRectMake(130, 350, 220, 75);
    UIButton * readButton = [[UIButton alloc] initWithFrame:readButtonFrame];
    
    CGRect collectionButtonFrame  = CGRectMake(130, 450, 220, 75);
    UIButton * collectionButton = [[UIButton alloc] initWithFrame:collectionButtonFrame];
    
    CGRect settingsButtonFrame  = CGRectMake(130, 550, 220, 75);
    UIButton * settingsButton = [[UIButton alloc] initWithFrame:settingsButtonFrame];
    
    NSArray * buttonsArray = @[readButton, collectionButton, settingsButton];
    for (UIButton * button in buttonsArray)
    {
        [button.layer setCornerRadius: 12.0f];
        [button.layer setBorderWidth:4.0f];
        [button setBackgroundColor:[UIColor colorWithRed:64/255.0 green:158/255.0 blue:255/255.0 alpha:0.8]];
        [button.titleLabel setFont:[UIFont fontWithName:@"Palatino-Bold" size: 24.0f]];
        [button setTitleColor:[UIColor colorWithRed:166/255.0f green:166/255.0f blue:255/255.0f alpha:0.8] forState:UIControlStateHighlighted];
    }
    
    [readButton setTitle:@"Продолжить" forState:UIControlStateNormal];
    [collectionButton setTitle:@"Коллекция" forState:UIControlStateNormal];
    [settingsButton setTitle:@"Настройки" forState:UIControlStateNormal];
    
    [readButton addTarget:self action:@selector(didPressReadButton:) forControlEvents:UIControlEventTouchUpInside];
    [collectionButton addTarget:self action:@selector(didPressCollectonButton:) forControlEvents:UIControlEventTouchUpInside];
    [settingsButton addTarget:self action:@selector(didPressSettingsButton:) forControlEvents:UIControlEventTouchUpInside];
    
    self.readButton = readButton;
   
    [self.view addSubview:readButton];
    [self.view addSubview:collectionButton];
    [self.view addSubview:settingsButton];
    
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Назад"
                                    style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    self.navigationItem.backBarButtonItem = backButton;
}

- (void) addMainBackgroundImage
{
    CGRect backFrame = self.view.frame;
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:backFrame];
    [imageView setImage:[UIImage imageNamed:@"mainViewImage.jpg"]];
    [imageView setAlpha:1.0f];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:imageView];
}




@end
