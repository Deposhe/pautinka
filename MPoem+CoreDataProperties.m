//
//  MPoem+CoreDataProperties.m
//  RSPautinka
//
//  Created by Сергей on 25.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MPoem+CoreDataProperties.h"

@implementation MPoem (CoreDataProperties)

@dynamic mobIsDefault;
@dynamic mobPoemContent;
@dynamic mobPoemName;
@dynamic mobImage;
@dynamic rlsAuthor;

@end
