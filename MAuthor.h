//
//  MAuthor.h
//  RSPautinka
//
//  Created by Сергей on 20.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MPoem;

NS_ASSUME_NONNULL_BEGIN

@interface MAuthor : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MAuthor+CoreDataProperties.h"
