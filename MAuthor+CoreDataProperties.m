//
//  MAuthor+CoreDataProperties.m
//  RSPautinka
//
//  Created by Сергей on 20.02.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MAuthor+CoreDataProperties.h"

@implementation MAuthor (CoreDataProperties)

@dynamic mobAuthorName;
@dynamic mobImage;
@dynamic rlsPoem;

@end
